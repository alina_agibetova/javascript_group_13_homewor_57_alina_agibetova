import { Component, Input, OnInit } from '@angular/core';
import { User } from '../shared/user.modal';
import { UserService } from '../shared/user.service';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css']
})
export class UsersComponent implements OnInit{
  @Input()users!: User[];

  constructor(public userService: UserService){}

  ngOnInit(): void{

  }

}
