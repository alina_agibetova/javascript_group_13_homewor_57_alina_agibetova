import { Component, EventEmitter, Output } from '@angular/core';
import { CATEGORIES } from '../shared/categories';
import { User } from '../shared/user.modal';
import { UserService } from '../shared/user.service';

@Component({
  selector: 'app-new-user',
  templateUrl: './new-user.component.html',
  styleUrls: ['./new-user.component.css']
})
export class NewUserComponent {
  @Output() newUser = new EventEmitter<User>();
  categories = CATEGORIES;
  userName = '';
  userMail = '';
  userActive = true;
  category = '';


  constructor(public userService: UserService){

  }

  createUser(event:Event){
    event.preventDefault();
    const user = new User(this.userName, this.userMail, this.userActive, this.category);
    this.newUser.emit(user);
  }

}
