import { User } from './user.modal';

export class UserService {
  users: User[] = [
    new User('Alina', 'alina@mail.ru', true, 'admin'),
    new User('Ermek', 'ermek@mail.ru', true, 'editor'),
  ]
}
