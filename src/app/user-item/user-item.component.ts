import { Component, EventEmitter, Input, Output } from '@angular/core';
import { User } from '../shared/user.modal';

@Component({
  selector: 'app-user-item',
  templateUrl: './user-item.component.html',
  styleUrls: ['./user-item.component.css']
})
export class UserItemComponent {
  @Input() user!: User;
  @Output() userClick = new EventEmitter<User>();


  onClick(){
    this.userClick.emit(this.user);
  }
}
