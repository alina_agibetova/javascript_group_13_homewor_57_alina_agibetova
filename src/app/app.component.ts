import { Component } from '@angular/core';
import { User } from './shared/user.modal';
import { UserService } from './shared/user.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  users: User[] = [];

  constructor(public userService: UserService){}

  onNewUser(user: User) {
    this.userService.users.push(user);
  }

}
